#!/bin/sh

if [ $# != 1 ]; then
    echo "Usage: $0 <config>"
    echo "Example: $0 ../foo/alertmanager.yml"
    exit 1
fi

# Handling for the moment : darwin and linux
OPERATING_SYSTEM=$(uname -s | tr '[:upper:]' '[:lower:]')
ALERTMANAGER_VERSION=0.21.0
ALERTMANAGER_DIR=alertmanager-${ALERTMANAGER_VERSION}.${OPERATING_SYSTEM}-amd64
ALERTMANAGER_ARCHIVE=${ALERTMANAGER_DIR}.tar.gz

if ! which wget; then
   echo "You need wget"
   exit 1
fi

if [ ! -f alertmanager ]; then
   wget https://github.com/prometheus/alertmanager/releases/download/v${ALERTMANAGER_VERSION}/${ALERTMANAGER_ARCHIVE}
   tar -xf ${ALERTMANAGER_ARCHIVE}
   cp ${ALERTMANAGER_DIR}/alertmanager alertmanager
   cp ${ALERTMANAGER_DIR}/amtool amtool
fi

ALERTMANAGER_YML=$(basename $1)
ALERTMANAGER_CONFIG_DIR=$(dirname $1)

DIR=$(mktemp -d)

cp -v ${ALERTMANAGER_CONFIG_DIR?}/* $DIR

./alertmanager --config.file ${DIR}/${ALERTMANAGER_YML}
