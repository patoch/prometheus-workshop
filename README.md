# Prometheus 101 workshop

Clone the GIT repository.

## Setup your environment

Make sure you have `Python3` and `pip` installed.

This workshop was tested on Linux and OSX but not on Windows. If you're on Windows, try to run the code on a Linux VM.

Run the following command to get the env setup done:
`source setup-env.sh`

## Change your Prometheus configuration

## Start Prometheus

Run `scripts/test-prometheus.sh configs/prometheus-workshop.yml`

Connect on the web interface from your browser: `http://localhost:9090`

```
Expected results:
- Being able to see the web interface
- Discover web interface
```

## Play with metrics

### The rate() function

`rate(prometheus_local_storage_chunk_ops_total[1m])`

### The sum aggregation operator

`sum(rate(prometheus_local_storage_chunk_ops_total[1m]))`

### Select by label

`rate(prometheus_local_storage_chunk_ops_total{type="persist"}[1m])`

`prometheus_target_interval_length_seconds{quantile="0.99"}`

### Aggregate by label

`sum(rate(http_request_duration_microseconds_count[5m])) by (job)`

`sum(rate(http_request_duration_microseconds_count[5m])) by (job, instance)`

### Arithmetic

`rate(http_request_duration_microseconds_sum[5m]) / rate(http_request_duration_microseconds_count[5m])`

`sum(rate(http_request_duration_microseconds_sum[5m])) by (job) / sum(rate(http_request_duration_microseconds_count[5m])) by (job)`

### Other metrics

```
prometheus_target_interval_length_seconds
prometheus_target_interval_length_seconds{quantile="0.99"}
prometheus_target_interval_length_seconds_count
up
```
### Documentation

All the functions are documented on the [Prometheus website](https://prometheus.io/docs/querying/functions/)

```
Expected results:
- Being able to query the metrics
```

## Launch my_app without Docker

### Add my_app target

Install requirements for myapp:
`pip install -r my_app/requirements.txt`

Launch myapp without Docker:
`cd my_app`
`export FLASK_APP="my_app.py"`
`ENV FLASK_ENV "production"`
`flask run myapp/my_app.py`

### Launch my_app with Docker

Build the Docker image:
`docker build -t my_app:latest my_app/.`

Launch the app:
`docker run -p 5000:5000  my_app:latest`

Check the metrics from your browser: `http://localhost:5000/metrics`

## Blackbox exporter

Run Blackbox exporter:
`cd blackbox_exporter`
`docker run --rm -d -p 9115:9115 --name blackbox_exporter -v `pwd`:/config prom/blackbox-exporter:master --config.file=/config/blackbox.yml`

## Configure Prometheus

Modify the prometheus configuration to scrape myapp metrics.

Add a new job called `job_name: 'my_app'`.

```
You can copy/paste the current job configuration and change `name` and `port` values.
```

Stop the current Prometheus instance, save your changes and start it again.

### Add Node exporter target

First launch the node exporter:
`scripts/node_exporter.sh`

Check the metrics on your browser: `http://localhost:9100/metrics`

Modify the prometheus configuration to scrape node_exporter metrics.

Add a new job called `job_name: 'node_exporter'`.

```
You can copy/paste the current job configuration and change `name` and `port` values.
```

Stop the current Prometheus instance, save your changes and start it again.

## Start Alertmanager

`scripts/test-alertmanager.sh configs/alertmanager-workshop.yml`

Check the web interface on your browser: `http://localhost:9093`

Modify the prometheus configuration to scrape alertmanager metrics.

Add a new job called `job_name: 'alertmanager'`.

```
You can copy/paste the current job configuration and change `name` and `port` values.
```

Stop the current Prometheus instance, save your changes and start it again.

```
Expected results:
- Being able to add a new static target
```

### Recording rules

Uncomment the `rules_files` parameter and value in the prometheus configuration: `configs/prometheus-workshop.yml`

Stop the current Prometheus instance, save your changes and start it again.

Check the rules in the Prometheus web interface: `http://localhost:9090/rules` and `http://localhost:9090/alerts`

Create your own rules for 2 metrics (in configs/myrules), save your changes and restart the prometheus instance.

Stop the current Prometheus instance, save your changes and start it again.

Check the rules in the Prometheus web interface: `http://localhost:9090/rules` and `http://localhost:9090/alerts`

### Alerting rules

Create your own rules for 2 metrics (in configs/myrules.rules), save your changes and restart the prometheus instance.

```
Expected results:
- Being able to create recording rules
- Being able to create alerting rules
- Being able to send alerts in Slack
```


## Test your config

Run `./promtool check config configs/prometheus-workshop.yml`

Check metric consistency: `curl -s http://localhost:9100/metrics | ./promtool check metrics`

```
Expected results:
- Being able to test the configuration
```

## Dashboarding with Grafana

Docker is required for that.

`docker run -d -p 3000:3000 --name grafana grafana/grafana:latest`

Go on http://localhost:3000

Default login/password: admin admin

### Add Prometheus Datasource