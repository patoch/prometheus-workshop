#!/bin/bash

virtualenv prometheus-workshop
source prometheus-workshop/bin/activate

pip install -r my_app/requirements.txt
